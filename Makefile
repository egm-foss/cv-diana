DOC  := EGM_CV_FR.tex
BUILD := build

all: 
	-latexmk  -pdf $(DOC) -output-directory=$(BUILD) 

purge:
	-rm -rf $(BUILD)
	
clean: purge
	-rm -f $(DOC:.tex=.pdf)
	-rm -f $(DOC:.tex=.out)
	-rm -f $(DOC:.tex=.log)

.PHONY: all purge clean
